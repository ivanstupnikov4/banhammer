#!/bin/bash

# Функция добавления правила для блокировки
ban() {
    local port="$1"
    local string="$2"
    local chain="${3:-"INPUT FORWARD OUTPUT"}"  # Если цепочка не указана, блокируем на всех

    # Проверка, что порт является числом
    if ! [[ $port =~ ^[0-9]+$ ]]; then
        echo "Ошибка: порт должен быть числом."
        return 1
    fi

    # Проверка допустимости значения chain
    for ch in $chain; do
        if [[ ! " INPUT FORWARD OUTPUT " =~ " $ch " ]]; then
            echo "Ошибка: недопустимое значение цепочки '$ch'. Допустимые значения: INPUT, FORWARD, OUTPUT."
            return 1
        fi
        sudo iptables -A "$ch" -p tcp --dport "$port" -m string --algo bm --string "$string" -j DROP
        echo "Последовательность '$string' заблокирована на порту $port в цепочке $ch."
    done
}

# Функция вывода всех блокировок
list() {
    local temp_file=$(mktemp)

    echo "| № | Chain | port | word |" >> $temp_file
    echo "|---|-------|------|------|" >> $temp_file

    local lines=$(sudo iptables -L -v -n --line-numbers)
    local count=1
    local current_chain=""

    while IFS= read -r line; do
        if [[ $line == Chain* ]]; then
            current_chain=$(echo "$line" | awk '{print $2}')
            continue
        fi

        if [[ $line == *STRING* ]]; then
            local port=$(echo "$line" | awk '{print $12}' | cut -d ':' -f 2)
            local word=$(echo "$line" | awk -F'"' '{print $2}')
            echo "| $count | $current_chain | $port | $word |" >> $temp_file
            count=$((count+1))
        fi
    done <<< "$lines"

    cat $temp_file | column -t -s "|"
    rm $temp_file
}

# Функция удаления правила блокировки
unban() {
    local port="$1"
    local string="$2"
    local chains="${3:-"INPUT FORWARD OUTPUT"}"

    if ! [[ $port =~ ^[0-9]+$ ]]; then
        echo "Ошибка: порт должен быть числом."
        return 1
    fi

    for ch in $chains; do
        if [[ ! " INPUT FORWARD OUTPUT " =~ " $ch " ]]; then
            echo "Ошибка: недопустимое значение цепочки '$ch'. Допустимые значения: INPUT, FORWARD, OUTPUT."
            return 1
        fi
        if sudo iptables -C "$ch" -p tcp --dport "$port" -m string --algo bm --string "$string" -j DROP &>/dev/null; then
            sudo iptables -D "$ch" -p tcp --dport "$port" -m string --algo bm --string "$string" -j DROP
            echo "Последовательность '$string' разблокирована на порту $port в цепочке $ch."
        else
            echo "Последовательность '$string' не найдена на порту $port в цепочке $ch."
        fi
    done
}

# Главное меню
if [ "$#" -lt 1 ]; then
    echo "Использование:"
    echo "$0 ban <port> <string> [chain]  - Заблокировать последовательность (если не указать цепочку то заблокируется на цепочках INPUT FORWARD OUTPUT)"
    echo "$0 list                        - Вывести все блокировки"
    echo "$0 unban <port> <string> [chain]- Убрать блокировку (если не указать цепочку то разблокируется на цепочках INPUT FORWARD OUTPUT)"
    exit 1
fi

case "$1" in
    ban)
        ban "$2" "$3" "$4"
        ;;
    list)
        list
        ;;
    unban)
        unban "$2" "$3" "$4"
        ;;
    *)
        echo "Неверная команда. Используйте 'ban', 'list' или 'unban'."
        ;;
esac
